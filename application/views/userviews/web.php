
<div class="jumbotron">
	<h3>
		
		Our services and charges
	</h3>
	<table class=" table table-hover">
		<tbody>
			<tr>
				<th>Service</th>
				<th>Age Specifications</th>
				<th>Charges</th>
			</tr>
		
			<tr>
				<td>Child Care: Day Care</td>
				<th>From 8 Months</th>
				<td> 300/- per day</td>
		
			</tr>
			<tr>
				<td></td>
				<th></th>
				<td>150/- per Half Day</td>
		
			</tr>
			<tr>
				<td></td>
				<th></th>
				<td>150/- per Month</td>
		
			</tr>

			<tr>
				<td>After School Stay and Holiday</td>
				<th>Up to 12yrs</th>
				<td>100/- per hour</td>
		
			</tr>
			
		</tbody>
	</table>
	<b>Special Notes:</b>
	Kindly note that Half Day is from 7:00am to 12:00pm or 12:00pm to 5:30pm
	<b>Extra services at an afforfable fee:</b>
	<ul>
		<li>Diapers @ 30/=</li>
		<li>Extra time : Full day stays extra 30mins free.</li>
	</ul>
<!-- <img src="<?php echo base_url()?>assets/images/t3.jpg" class="img-responsive img-circle"> -->
</div>

<?php

	$this->load->view('userviews/tabfooter');
?>