
	
			<div class="col-xs-9">
				<div class="panel panel-primary">
					<div class="panel-heading">
				<h4 class="panel-title">Email Us</h4>


						</div>
					
					<div class="panel-body">
						<div class="panel-body col-xs-6">
					
					<form class="form-horizontal" id="conform">
						<div class="form-group">
							<label class="col-sm-2 control-label"for="inputName">Full Name

							</label>
							<div class="col-md-10">
							<input class="form-control" type="text" id="name" placeholder="Enter your Full name">
						</div>

						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputEmail">Email

							</label>
							<div class="col-md-10">
							<input class="form-control"type="email" id="email" placeholder="Enter your Email">
						</div>

						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputSub">Subject

							</label>
							<div class="col-md-10">
							<input class="form-control"type="text" id="subject" placeholder="Enter the Subject/Topic">
						</div>

						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputBody">Body

							</label>
							<div class="col-md-10">
							<!-- <input class="form-control" type="password" id="pass"placeholder="Enter Password"> -->
							<textarea class="form-control" rows="3" id="text"></textarea>
								</div>

						</div>
						<!-- <div>
							<label class="checkbox col-md-offset-2" >
								<div class="col-md-10">
								<input  type="checkbox" value=""> Remember me
							</div> -->
							
					 <div>
							<div class="col-sm-10 col-md-offset-2">
							<button class="btn btn-primary" type="button" id="formsubmit">Send</button>
						</div>

						</div> 

			 </form>
		</div>

	</div>


	</div>
</div>
<div class="col-xs-3">
	<div class="panel panel-primary">
					<div class="panel-heading">
				<h4 class="panel-title">Address</h4>


						</div>
					
					<div class="panel-body">
			 		<address>
				  <strong>Buru Buru Phase 1,</strong><br>
				  Ol Pogoni Road,<br>
				  Bamboo Court<br>
				  House No_ 89.<br><br><br>
				  <abbr title="Phone">Call:</abbr> (+254-721) 778-634 <br>
				  <abbr title="Phone">Call:</abbr> (+254-739) 879825 <br><br>
				  <p style="word-wrap: break-word;">
				  Email us : <strong>classic.ladaycare@gmail.com</strong>
				  </p>
				</address>
			</div>
		</div>
	</div>
<?php

	$this->load->view('userviews/tabfooter');
?>
	<script>
	function validate(id){

		if($("#"+id).val()==null || $("#"+id).val()==""){
			var div= $("#"+id).closest("div");
			div.removeClass("has-success");
			$("#gly"+id).remove();
			div.addClass("has-error");
			div.append('<span id="gly'+id+'" class="glyphicon glyphicon-remove form-control-feedback"></span>');
			return false;
		}else{
			var div= $("#"+id).closest("div");
			div.removeClass("has-error");
			div.addClass("has-success has-feedback");
			$("#gly"+id).remove();
			div.append('<span id="gly'+id+'" class="glyphicon glyphicon-ok form-control-feedback"></span>');
			

			return true;
		}
	}
	$(document).ready(


		function()
		{

			$("#formsubmit").click(function(){
				if(!validate("name")){
					return false;
				}
				if(!validate("email")){
					return false;
				}
				if(!validate("subject")){
					return false;
				}
				if(!validate("text")){
					return false;
				}
				


				$("form#conform").submit();
			});
		}



		);

	</script>